#!/bin/bash
CONTAINER_NAME="bsc-mainnet"

echo "Stopping BSC geth (mainnet) container: $CONTAINER_NAME ..."
exec docker stop --time=120 $CONTAINER_NAME
#exec docker kill --signal=HUP $CONTAINER_NAME

