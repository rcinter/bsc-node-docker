#!/bin/bash

CONTAINER_NAME="bsc-mainnet"
IMAGE_NAME="afanasjev/binance-smart-chain-node:latest"
HOST_CHAIN_PATH="/media/storage/bsc/.ethereum/mainnet"

# Geth params
NETWORK="mainnet"
CHAIN_ID="56"
SYNCMODE="snap"
CACHE="8192"
APIS="eth,net,web3,txpool"
MAX_PEERS="50"
PORT_RPC="8515"
PORT_WS="8516"
PORT_DISCOVERY="30311"

CONTAINER_GETH_ARGS="--networkid $CHAIN_ID
		--syncmode $SYNCMODE
		--datadir /root/.ethereum
		--cache $CACHE
		--maxpeers $MAX_PEERS
		--http --http.addr 0.0.0.0
		--http.api $APIS
		--http.port $PORT_RPC
		--http.vhosts '*'
		--http.corsdomain chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn
		--ws --ws.addr 0.0.0.0
		--ws.api $APIS
		--ws.port $PORT_WS
		--ws.origins '*'
		--diffsync"

START_CONTAINER="docker run -itd
			-e NETWORK=$NETWORK
			-p $PORT_DISCOVERY:$PORT_DISCOVERY
			-p $PORT_DISCOVERY:$PORT_DISCOVERY/udp
			-p $PORT_RPC:$PORT_RPC
			-p $PORT_WS:$PORT_WS
			-v $HOST_CHAIN_PATH:/root/.ethereum
			--name $CONTAINER_NAME
				$IMAGE_NAME
					bsc $CONTAINER_GETH_ARGS"

echo -e "\e[44mStarting BSC geth container $CONTAINER_NAME on $HOST_CHAIN_PATH ...\e[0m"
exec $START_CONTAINER
