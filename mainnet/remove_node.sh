#!/bin/bash
CONTAINER_NAME="bsc-mainnet"

echo "Removing BSC geth container: $CONTAINER_NAME ..."
exec docker rm -f $CONTAINER_NAME

