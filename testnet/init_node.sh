#!/bin/bash
CONTAINER_NAME="bsc-testnet"
HOST_CHAIN_PATH="/media/storage/bsc/.ethereum/testnet" 
HOST_SHARE_PATH="$(pwd)/share"
DATA_DIR="/root/.ethereum"
IMAGE_NAME="afanasjev/binance-smart-chain-node:latest"

START_CONTAINER="docker run -it \
			-v $HOST_CHAIN_PATH:$DATA_DIR \
			-v $HOST_SHARE_PATH/testnet.json:$DATA_DIR/testnet.json \
			-v $HOST_SHARE_PATH/static-nodes.json:$DATA_DIR/static-nodes.json \
				$IMAGE_NAME \
					--datadir=$DATA_DIR init $DATA_DIR/testnet.json"
echo -e "\e[44mStarting Geth container $CONTAINER_NAME on $HOST_CHAIN_PATH ...\e[0m"
eval $START_CONTAINER
