#!/bin/bash
CONTAINER_NAME="bsc-testnet"

echo -e "\e[44mRunning container $CONTAINER_NAME logs ...\e[0m"
exec docker logs --tail 30 --follow $CONTAINER_NAME

