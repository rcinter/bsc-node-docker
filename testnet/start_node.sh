#!/bin/bash
CONTAINER_NAME="bsc-testnet"
IMAGE_NAME="afanasjev/binance-smart-chain-node:latest"
HOST_CHAIN_PATH="/media/storage/bsc/.ethereum/testnet"

# Geth params
NETWORK="testnet"
CHAIN_ID="97"
SYNCMODE="snap"
CACHE="1024"
APIS="eth,net,web3,txpool"
PORT_RPC="8545"
PORT_WS="8546"
PORT_DISCOVERY="30341"

CONTAINER_GETH_ARGS="--networkid $CHAIN_ID
		--syncmode $SYNCMODE
		--datadir /root/.ethereum
		--cache $CACHE
		--http --http.addr 0.0.0.0
		--http.api $APIS
		--http.port $PORT_RPC
		--http.vhosts '*'
		--http.corsdomain chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn
		--ws --ws.addr 0.0.0.0
		--ws.api $APIS
		--ws.port $PORT_WS
		--ws.origins '*'"

START_CONTAINER="docker run -itd
			--restart unless-stopped
			-e NETWORK=$NETWORK
			-p $PORT_DISCOVERY:$PORT_DISCOVERY
			-p $PORT_DISCOVERY:$PORT_DISCOVERY/udp
			-p $PORT_RPC:$PORT_RPC
			-p $PORT_WS:$PORT_WS
			-v $HOST_CHAIN_PATH:/root/.ethereum
			--name $CONTAINER_NAME
				$IMAGE_NAME
					$CONTAINER_GETH_ARGS"

echo -e "\e[44mStarting BSC geth container $CONTAINER_NAME on $HOST_CHAIN_PATH ...\e[0m"
exec $START_CONTAINER

