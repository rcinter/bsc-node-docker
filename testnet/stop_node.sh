#!/bin/bash
CONTAINER_NAME="bsc-testnet"

echo "Stopping BSC geth (mainnet) container: $CONTAINER_NAME ..."
exec docker stop $CONTAINER_NAME

