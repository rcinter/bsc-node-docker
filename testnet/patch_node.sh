#!/bin/bash
HOST_CHAIN_PATH="/media/storage/bsc/.ethereum/testnet"
USERNAME="afanasjev"
USER_ID=$(id -u $USERNAME)
GROUP_ID=$(id -g $USERNAME)

GETH_IPC_PATH="$HOST_CHAIN_PATH/geth.ipc"
GETH_IPC_MOD="chown $USER_ID:$GROUP_ID $GETH_IPC_PATH"

echo -e "\e[44mChanging $GETH_IPC_PATH owner to $USERNAME ($USER_ID:$GROUP_ID) ...\e[0m"
eval $GETH_IPC_MOD
