#!/bin/bash
CONTAINER_NAME="bsc-testnet"

echo -e "\e[44mRunning geth console on $CONTAINER_NAME ...\e[0m"
exec docker exec -it $CONTAINER_NAME bsc attach /root/.ethereum/geth.ipc
#exec geth attach /media/storage/.ethereum/mainnet/geth.ipc

